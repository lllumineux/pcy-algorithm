import csv
import itertools

from config import MINIMAL_SUPPORT_LVL


def main():
    baskets = dict()
    products = set()

    with open('../data/transactions.csv', newline='') as csvfile:
        transactions_reader = csv.reader(csvfile, delimiter=';')
        _ = next(transactions_reader)  # workaround to omit the header

        for row in transactions_reader:
            product_code, basket_id = row[0], int(row[1])
            products.add(product_code)
            if basket_id not in baskets.keys():
                baskets[basket_id] = []
            baskets[basket_id].append(product_code)

    products = list(products)
    products_number = len(products)

    products_frequency = {product: 0 for product in products}
    hash_bucket = [{'count': 0, 'pairs': []} for _ in range(products_number)]

    for _, basket_products in baskets.items():
        # pass #1 logic
        for p in basket_products:
            products_frequency[p] += 1

        # pass #2 logic
        for p1, p2 in itertools.combinations(basket_products, 2):
            p1_id, p2_id = products.index(p1) + 1, products.index(p2) + 1
            index = (p1_id + p2_id) % products_number
            hash_bucket[index]['count'] += 1
            hash_bucket[index]['pairs'].append((p1, p2))

    # check pass #1 + pass #2 conditions
    with open('../res.txt', 'w+') as f:
        for info in hash_bucket:
            for p1, p2 in info['pairs']:
                if (
                    info['count'] >= MINIMAL_SUPPORT_LVL and
                    products_frequency[p1] >= MINIMAL_SUPPORT_LVL and
                    products_frequency[p1] >= MINIMAL_SUPPORT_LVL
                ):
                    f.writelines([f'{p1} {p2}\n'])


if __name__ == '__main__':
    main()
